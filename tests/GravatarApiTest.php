<?php
/**
 * @category Jojo81
 * @package Gravatar
 * @subpackage Test
 * @author Joost Nijhuis <jnijhuis81@gmail.com>
 * @license MIT
 */
namespace Jojo1981\Gravatar\GravatarApi\Tests;

use Jojo1981\Gravatar\GravatarApi;
use Jojo1981\Gravatar\Config\GravatarConfig;

/**
 * @category Jojo81
 * @package Gravatar
 * @subpackage Test
 * @author Joost Nijhuis <jnijhuis81@gmail.com>
 * @license MIT
 *
 * Jojo1981\Gravatar\GravatarApi\Tests\GravatarApiTest
 */
class GravatarApiTest extends \PHPUnit_Framework_TestCase
{
    /**
     * testExistsAvatarForEmailAddressShouldReturnFalse
     */
    public function testExistsAvatarForEmailAddressShouldReturnFalse()
    {
        $config = new GravatarConfig();
        $api = new GravatarApi($config);

        $result = $api->existsAvatarForEmailAddress('jan@jansen.nl');

        $this->assertFalse($result);
    }

    /**
     * testExistsAvatarForEmailAddressShouldReturnTrue
     */
    public function testExistsAvatarForEmailAddressShouldReturnTrue()
    {
        $config = new GravatarConfig();
        $api = new GravatarApi($config);

        $result = $api->existsAvatarForEmailAddress('jnijhuis81@gmail.com');

        $this->assertTrue($result);
    }
}
