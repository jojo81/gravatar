<?php
/**
 * @category Jojo81
 * @package Gravatar
 * @subpackage Test
 * @author Joost Nijhuis <jnijhuis81@gmail.com>
 * @license MIT
 */

if (!file_exists(__DIR__ . '/../vendor/autoload.php')) {
    echo <<<'EOT'
You must set up the project dependencies, run the following commands:
wget http://getcomposer.org/composer.phar
php composer.phar install
EOT;
    exit(1);
}

require __DIR__ . '/../vendor/autoload.php';
