<?php
/**
 * @category Jojo81
 * @package Gravatar
 * @author Joost Nijhuis <jnijhuis81@gmail.com>
 * @license MIT
 */
namespace Jojo1981\Gravatar;

use Jojo1981\Gravatar\Config\GravatarConfigInterface;

/**
 * @category Jojo81
 * @package Gravatar
 * @author Joost Nijhuis <jnijhuis81@gmail.com>
 * @license MIT
 *
 * Jojo1981\Gravatar\GravatarApi
 */
class GravatarApi
{
    /**
     * @var GravatarConfigInterface
     */
    protected $config;

    /**
     * Constructor
     *
     * @param GravatarConfigInterface $config
     */
    public function __construct(GravatarConfigInterface $config)
    {
        $this->config = $config;
    }

    /**
     * Get avatar url using the email address
     *
     * @param string $emailAddress
     * @param null|int $size
     * @param null|string $rating
     * @param null|string $default
     * @param null|bool $secure
     * @return string
     */
    public function getAvatarUrlUsingEmailAddress(
        $emailAddress,
        $size = null,
        $rating = null,
        $default = null,
        $secure = null
    ) {
        $hash = $this->getHashForEmailAddress($emailAddress);

        return $this->getAvatarUrlUsingHash(
            $hash,
            $size,
            $rating,
            $default,
            $secure
        );
    }

    /**
     * Get avatar url using the hashed email address
     *
     * @param string $hash
     * @param null|int $size
     * @param null|string $rating
     * @param null|string $default
     * @param null|bool $secure
     * @return string
     */
    public function getAvatarUrlUsingHash(
        $hash,
        $size = null,
        $rating = null,
        $default = null,
        $secure = null
    ) {
        $size = $size ?: $this->config->getSize();
        $rating = $rating ?: $this->config->getRating();
        $secure = $secure ?: $this->config->isSecure();
        $default = $default ?: $this->config->getDefault();

        $scheme = $secure ? 'https://secure' : 'http://www';
        $urlPattern = '%s.gravatar.com/avatar/%s?%s';

        $map = array(
            's' => $size,
            'r' => $rating,
            'd' => $default
        );

        return sprintf(
            $urlPattern,
            $scheme,
            $hash,
            http_build_query(array_filter($map))
        );
    }

    /**
     * Check if there is an avatar at Gravatar for the passed
     * email address
     *
     * @param string $emailAddress
     * @return bool
     */
    public function existsAvatarForEmailAddress($emailAddress)
    {
        $url = $this->getAvatarUrlUsingEmailAddress(
            $emailAddress,
            null,
            null,
            GravatarConfigInterface::DEFAULT_TYPE_404
        );

        $file_headers = @get_headers($url);
        if (!isset($file_headers[0])) {
            return false;
        }

        return $file_headers[0] != 'HTTP/1.1 404 Not Found';
    }

    /**
     * Get has for passed email address
     *
     * @param string $emailAddress
     * @return string
     */
    public function getHashForEmailAddress($emailAddress)
    {
        return md5(strtolower(trim($emailAddress)));
    }
}
