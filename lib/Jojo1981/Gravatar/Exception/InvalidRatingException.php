<?php
/**
 * @category Jojo81
 * @package Gravatar
 * @subpackage Exception
 * @author Joost Nijhuis <jnijhuis81@gmail.com>
 * @license MIT
 */
namespace Jojo1981\Gravatar\Exception;

/**
 * @category Jojo81
 * @package Gravatar
 * @subpackage Exception
 * @author Joost Nijhuis <jnijhuis81@gmail.com>
 * @license MIT
 *
 * Jojo1981\Gravatar\Exception\InvalidRatingException
 */
class InvalidRatingException extends \Exception
{

}
