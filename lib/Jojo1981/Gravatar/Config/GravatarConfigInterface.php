<?php
/**
 * @category Jojo81
 * @package Gravatar
 * @subpackage Config
 * @author Joost Nijhuis <jnijhuis81@gmail.com>
 * @license MIT
 */
namespace Jojo1981\Gravatar\Config;

use Jojo1981\Gravatar\Exception\InvalidRatingException;
use Jojo1981\Gravatar\Exception\InvalidSizeException;
use Jojo1981\Gravatar\Exception\InvalidDefaultException;

/**
 * @category Jojo81
 * @package Gravatar
 * @subpackage Config
 * @author Joost Nijhuis <jnijhuis81@gmail.com>
 * @license MIT
 *
 * Jojo1981\Gravatar\Config\GravatarConfigInterface
 */
interface GravatarConfigInterface
{
    /**
     * suitable for display on all websites with any audience type.
     */
    const RATING_TYPE_G  = 'g';

    /**
     * may contain rude gestures, provocatively dressed individuals,
     * the lesser swear words, or mild violence.
     */
    const RATING_TYPE_PG = 'pg';

    /**
     * may contain such things as harsh profanity, intense violence,
     * nudity, or hard drug use.
     */
    const RATING_TYPE_R  = 'r';

    /**
     * may contain hardcore sexual imagery or extremely disturbing
     * violence.
     */
    const RATING_TYPE_X  = 'x';

    /**
     * Do not load any image if none is associated with the email
     * hash, instead return an HTTP 404 (File Not Found) response
     */
    const DEFAULT_TYPE_404 = '404';

    /**
     * Mystery-man a simple, cartoon-style silhouetted outline of
     * a person (does not vary by email hash)
     */
    const DEFAULT_TYPE_MYSTERY_MAN = 'mm';

    /**
     * a geometric pattern based on an email hash
     */
    const DEFAULT_TYPE_IDENTICON   = 'identicon';

    /**
     * A generated 'monster' with different colors, faces, etc
     */
    const DEFAULT_TYPE_MONSTER_ID = 'monsterid';

    /**
     * Generated faces with differing features and backgrounds
     */
    const DEFAULT_TYPE_WAVATAR = 'wavatar';

    /**
     * Awesome generated, 8-bit arcade-style pixelated faces
     */
    const DEFAULT_TYPE_RETRO = 'retro';

    /**
     * A transparent PNG image (border added to HTML below for
     * demonstration purposes)
     */
    const DEFAULT_TYPE_BLANK = 'blank';

    /**
     * @param string $rating
     * @throws InvalidRatingException
     * @return GravatarConfigInterface
     */
    public function setRating($rating);

    /**
     * Get the configured rating
     *
     * @return string
     */
    public function getRating();

    /**
     * Set if the avatar image must be retrieved using SSL (https) or
     * without using SSL (http)
     *
     * @param bool $secure
     * @return GravatarConfigInterface
     */
    public function setIsSecure($secure);

    /**
     * Return true whether avatar image must be retrieved using SSL (https)
     *
     * @return bool
     */
    public function isSecure();

    /**
     * Set size (pixels)
     *
     * @param int $size
     * @throws InvalidSizeException
     * @return GravatarConfigInterface
     */
    public function setSize($size);

    /**
     * Get configured size (pixels)
     *
     * @return int
     */
    public function getSize();

    /**
     * Set the default, what to return in case
     * there is not an avatar for the passed email address
     *
     * @param null|string $default
     * @throws InvalidDefaultException
     * @return GravatarConfigInterface
     */
    public function setDefault($default);

    /**
     * Get the configured default, what to return in case
     * there is not an avatar for the passed email address
     *
     * @return null|string
     */
    public function getDefault();
}
