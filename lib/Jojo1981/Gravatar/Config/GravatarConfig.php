<?php
/**
 * @category Jojo81
 * @package Gravatar
 * @subpackage Config
 * @author Joost Nijhuis <jnijhuis81@gmail.com>
 * @license MIT
 */
namespace Jojo1981\Gravatar\Config;

use Jojo1981\Gravatar\Exception\InvalidSizeException;
use Jojo1981\Gravatar\Exception\InvalidRatingException;
use Jojo1981\Gravatar\Exception\InvalidDefaultException;

/**
 * @category Jojo81
 * @package Gravatar
 * @subpackage Config
 * @author Joost Nijhuis <jnijhuis81@gmail.com>
 * @license MIT
 *
 * Jojo1981\Gravatar\Config\GravatarConfig
 */
class GravatarConfig implements GravatarConfigInterface
{
    /**
     * Contains the valid values for rating type which
     * Gravatar understands
     *
     * @var array
     */
    protected $validRatings = array(
        self::RATING_TYPE_G,
        self::RATING_TYPE_PG,
        self::RATING_TYPE_R,
        self::RATING_TYPE_X
    );

    /**
     * Contains all the valid values for default type which
     * Gravatar understands
     *
     * @var array
     */
    protected $validDefaults = array(
        self::DEFAULT_TYPE_404,
        self::DEFAULT_TYPE_MYSTERY_MAN,
        self::DEFAULT_TYPE_IDENTICON,
        self::DEFAULT_TYPE_BLANK,
        self::DEFAULT_TYPE_MONSTER_ID,
        self::DEFAULT_TYPE_RETRO,
        self::DEFAULT_TYPE_WAVATAR
    );

    /**
     * The size in pixels for the x and y axis
     *
     * @var int
     */
    protected $size = 80;

    /**
     * The rating type to use.
     *
     * @var string
     */
    protected $rating = self::RATING_TYPE_G;

    /**
     * If the avatar must be retrieved using SSL (https)
     *
     * @var bool
     */
    protected $useSSL = false;

    /**
     * @var null|string
     */
    protected $default = null;

    /**
     * {@inheritDoc}
     */
    public function setRating($rating)
    {
        if (!array_key_exists($rating, $this->validRatings)) {
            throw new InvalidRatingException(sprintf(
                'Invalid rating: %s used, valid rating is one of: %s',
                $rating,
                implode(', ', $this->validRatings)

            ));
        }
        $this->rating = $rating;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * {@inheritDoc}
     */
    public function setIsSecure($secure)
    {
        $this->useSSL = $secure;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function isSecure()
    {
        return $this->useSSL;
    }

    /**
     * {@inheritDoc}
     */
    public function setSize($size)
    {
        $size = (int) $size;
        if ($size < 1 || $size > 2048) {
            throw new InvalidSizeException(sprintf(
                'Invalid size: %s used, valid size must be => 1 and =< 2048',
                $size
            ));
        }
        $this->size = $size;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * {@inheritDoc}
     */
    public function setDefault($default)
    {
        if (!array_key_exists($default, $this->validDefaults)) {
            throw new InvalidDefaultException(sprintf(
                'Invalid default: %s used, valid default is one of: %s',
                $default,
                implode(', ', $this->validDefaults)

            ));
        }
        $this->default = $default;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getDefault()
    {
        return $this->default;
    }
}
